#include <testing.h>
#include <swap_sort.h>
#include <limits>
#include <vector>
#include <algorithm>

namespace tests {

void SwapTest() {
    {
        int a = 5, b = 7;
        Swap(&a, &b);
        ASSERT_EQ(7, a);
        ASSERT_EQ(5, b);
    }
    {
        int a = std::numeric_limits<int>::min();
        int b = std::numeric_limits<int>::max();
        Swap(&a, &b);
        ASSERT_EQ(a, std::numeric_limits<int>::max());
        ASSERT_EQ(b, std::numeric_limits<int>::min());
    }
    {
        int a = 1, b = 1;
        Swap(&a, &b);
        ASSERT_EQ(1, a);
        ASSERT_EQ(1, b);
    }
}

void SortAll() {
    std::vector<int> p{1, 2, 3};
    do {
        int a = p[0], b = p[1], c = p[2];
        Sort3(&a, &b, &c);
        ASSERT_EQ(1, a);
        ASSERT_EQ(2, b);
        ASSERT_EQ(3, c);
    } while (std::next_permutation(p.begin(), p.end()));
}

void SortEq() {
    {
        int a = 1, b = 1, c = 1;
        Sort3(&a, &b, &c);
        ASSERT_EQ(1, a);
        ASSERT_EQ(1, b);
        ASSERT_EQ(1, c);
    }
    {
        int a = 2, b = 4, c = 2;
        Sort3(&a, &b, &c);
        ASSERT_EQ(2, a);
        ASSERT_EQ(2, b);
        ASSERT_EQ(4, c);
    }
    {
        int a = 3, b = 3, c = 1;
        Sort3(&a, &b, &c);
        ASSERT_EQ(1, a);
        ASSERT_EQ(3, b);
        ASSERT_EQ(3, c);
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(SwapTest);
    RUN_TEST(SortAll);
    RUN_TEST(SortEq);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
