cmake_minimum_required(VERSION 2.8)
project(entrance)

if (TEST_SOLUTION)
  include_directories(../private/entrance)
endif()

include(../common.cmake)

set(SRC test.cpp)

if (ENABLE_PRIVATE_TESTS)
    set(SRC ../private/entrance/test.cpp)
endif()

add_executable(test_entrance ${SRC})
