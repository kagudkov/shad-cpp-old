#pragma once

#include <list>
#include <string>
#include <unordered_map>

class LruCache {
public:
    LruCache(size_t max_size) {}

    void Set(const std::string& key, const std::string& value) {}

    bool Get(const std::string& key, std::string* value) { return false; }

private:
};
