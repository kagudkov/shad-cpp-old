cmake_minimum_required(VERSION 2.8)
project(lru-cache)

if (TEST_SOLUTION)
  include_directories(../private/lru-cache)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_lru_cache
  test.cpp
  ../commons/catch_main.cpp)
