cmake_minimum_required(VERSION 3.0)
project(fast-dither)

find_package(PNG REQUIRED)
include_directories(${PNG_INCLUDE_DIRS})

if (TEST_SOLUTION)
  include_directories(../private/fast-dither)
endif()

include(../common.cmake)

add_executable(test_fast_dither
  test.cpp
  ../commons/catch_main.cpp)
target_link_libraries(test_fast_dither ${PNG_LIBRARY})
