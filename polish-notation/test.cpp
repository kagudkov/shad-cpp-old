#include <testing.h>
#include <polish_notation.h>
#include <string>

namespace tests {

void Short() {
    ASSERT_EQ(1, EvaluateExpression("1"));
}

void SomeTests() {
    ASSERT_EQ(5, EvaluateExpression("2 3 +"));
    ASSERT_EQ(6, EvaluateExpression("2 2 * 2 +"));
    ASSERT_EQ(1, EvaluateExpression("10 3 3 * -"));
    ASSERT_EQ(0, EvaluateExpression("-3 11 + -8 + 4 *"));
    ASSERT_EQ(20, EvaluateExpression("-3 -0 - 3 + 1 - -19 * 1 +"));
    ASSERT_EQ(15, EvaluateExpression("1 2 3 4 5 + + + +"));
    ASSERT_EQ(-9, EvaluateExpression("1 -1 2 -2 3 -3 * - + * *"));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Short);
    RUN_TEST(SomeTests);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
