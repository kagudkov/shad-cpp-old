cmake_minimum_required(VERSION 2.8)
project(bin-pow)

if (TEST_SOLUTION)
  include_directories(../private/bin-pow)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_binpow test.cpp)
