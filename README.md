# Курс C++

Это основной репозиторий курса. Инструкции по настройке окружения можно найти [на вики](https://wiki.school.yandex.ru/shad/groups/2017/Semester1/LearningC/). Структура семинарских задач приведена [в тестовой задаче](https://best-cpp-course-ever.ru/prime/shad-cpp/tree/master/multiplication). Задачи типа crashme описаны [здесь](https://best-cpp-course-ever.ru/prime/shad-cpp/blob/master/crash_readme.md).

Перемещаться по задачам и следить за дедлайнами можно тут: https://ctf.best-cpp-course-ever.ru/
