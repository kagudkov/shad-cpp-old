cmake_minimum_required(VERSION 2.8)
project(diff-pairs)

if (TEST_SOLUTION)
  include_directories(../private/diff-pairs)
endif()

include(../common.cmake)

set(SRC test.cpp)

if (ENABLE_PRIVATE_TESTS)
    set(SRC ../private/diff-pairs/test.cpp)
endif()

add_executable(test_diff_pairs ${SRC})
