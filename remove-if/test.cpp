#include <testing.h>
#include <util.h>
#include <strict_iterator.h>
#include <remove_if.h>

#include <string>
#include <vector>

bool IsEmpty(const std::string& s) {
    return s.empty();
}

namespace tests {

void Simple() {
    std::vector<std::string> data{"aba", "", "caba", "", ""};
    auto first = MakeStrict(data.begin(), data.begin(), data.end());
    auto last = MakeStrict(data.begin(), data.end(), data.end());
    auto it = RemoveIf(first, last, IsEmpty);

    auto expected = MakeStrict(data.begin(), data.begin() + 2, data.end());
    ASSERT_EQ(true, expected == it);
    ASSERT_EQ("aba", data[0]);
    ASSERT_EQ("caba", data[1]);
}

void Empty() {
    std::vector<std::string> data;
    auto first = MakeStrict(data.begin(), data.begin(), data.end());
    auto last = MakeStrict(data.begin(), data.end(), data.end());
    ASSERT_EQ(true, first == RemoveIf(first, last, IsEmpty));
}

void Long() {
    RandomGenerator rnd(85475);
    const int count = 1e5;
    const int val = 1e9;
    std::vector<int> elems(count);
    for (int& x : elems)
        x = rnd.GenInt(-val, val);
    auto first = MakeStrict(elems.begin(), elems.begin(), elems.end());
    auto last = MakeStrict(elems.begin(), elems.end(), elems.end());
    auto old_elems = elems;

    auto new_end = RemoveIf(first, last, [](int x) {
        return x % 2 == 0;
    });

    auto cur_it = old_elems.begin();
    for (auto it = first; it != new_end; ++it) {
        while (*cur_it % 2 == 0)
            ++cur_it;
        ASSERT_EQ(*cur_it, *it);
        ++cur_it;
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(Long);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
