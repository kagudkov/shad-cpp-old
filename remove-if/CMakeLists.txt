cmake_minimum_required(VERSION 2.8)
project(remove-if)

if (TEST_SOLUTION)
  include_directories(../private/remove-if)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_remove_if test.cpp)
