cmake_minimum_required(VERSION 2.8)
project(long-sum)

if (TEST_SOLUTION)
  include_directories(../private/long-sum)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_long_sum test.cpp)
