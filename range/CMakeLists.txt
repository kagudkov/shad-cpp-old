cmake_minimum_required(VERSION 2.8)
project(range)

if (TEST_SOLUTION)
  include_directories(../private/range)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_range test.cpp)
