cmake_minimum_required(VERSION 2.8)
project(multiplication)

if (TEST_SOLUTION)
  include_directories(../private/multiplication)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_multiplication test.cpp)
