#include <testing.h>
#include <multiplication.h>

namespace tests {

void SimpleTest() {
    ASSERT_EQ(6, Multiply(2, 3));
}

void AdvancedTest() {
    ASSERT_EQ(-100000018299999867LL, Multiply(999999993, -100000019));
}

void TestAll() {
    StartTesting();
    RUN_TEST(SimpleTest);
    RUN_TEST(AdvancedTest);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
