# iojoke

Это задача типа [crashme](https://best-cpp-course-ever.ru/prime/shad-cpp/blob/master/crash_readme.md).

Исходный код находится в файле iojoke.cpp. Исполяемый файл получен командой

```
g++ -std=c++14 iojoke.cpp -o iojoke
```

_На сервере стоит glibc версии 2.23_

 *Автор:* Данила Кутенин [@Danlark](https://t.me/Danlark)
