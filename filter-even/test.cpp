#include <testing.h>
#include <util.h>
#include <filter_even.h>
#include <vector>

namespace tests {

void Simple() {
    {
        std::vector<int> test{1, 4, 3, 2, 6, 7, 0};
        std::vector<int> expected{4, 2, 6, 0};
        FilterEven(&test);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{2, 1, 3, 5};
        std::vector<int> expected{2};
        FilterEven(&test);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{7, 3, 5, 4, 6};
        std::vector<int> expected{4, 6};
        FilterEven(&test);
        ASSERT_EQ(expected, test);
    }
}

void Empty() {
    {
        std::vector<int> test, expected;
        FilterEven(&test);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{1, -5, -3, 3, 7};
        std::vector<int> expected;
        FilterEven(&test);
        ASSERT_EQ(expected, test);
    }
}

void Big() {
    const int size = 1e6;
    RandomGenerator rnd(3784657);
    auto test = rnd.GenPermutation(size);
    auto expected = test;
    FilterEven(&test);
    expected.erase(std::remove_if(expected.begin(), expected.end(), [](int x) {return x % 2 != 0;}), expected.end());
    ASSERT_EQ(expected, test);
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
