cmake_minimum_required(VERSION 2.8)
project(ring-buffer)

if (TEST_SOLUTION)
  include_directories(../private/ring-buffer)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_ring_buffer test.cpp)
