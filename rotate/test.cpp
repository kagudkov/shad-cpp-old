#include <testing.h>
#include <util.h>
#include <rotate.h>
#include <vector>
#include <algorithm>

namespace tests {

void Simple() {
    {
        std::vector<int> test{1, 4, 3, 2, 6, 7, 0};
        std::vector<int> expected{3, 2, 6, 7, 0, 1, 4};
        Rotate(&test, 2);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{2, 1, 3, 5};
        std::vector<int> expected{5, 2, 1, 3};
        Rotate(&test, 3);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{1, 2};
        std::vector<int> expected{1, 2};
        Rotate(&test, 0);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{1, 2, 3};
        std::vector<int> expected{1, 2, 3};
        Rotate(&test, 3);
        ASSERT_EQ(expected, test);
    }
}

void Empty() {
    {
        std::vector<int> test, expected;
        Rotate(&test, 0);
        ASSERT_EQ(expected, test);
    }
    {
        std::vector<int> test{0};
        std::vector<int> expected{0};
        Rotate(&test, 1);
        ASSERT_EQ(expected, test);
    }
}

void Big() {
    const int size = 1e6;
    int to = 1e9;
    int from = -to;
    int shift = 1e5;
    RandomGenerator rnd(7345422);
    auto test = rnd.GenIntegralVector(size, from, to);
    auto expected = test;
    Rotate(&test, shift);
    std::rotate(expected.begin(), expected.begin() + shift, expected.end());
    ASSERT_EQ(expected, test);
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
